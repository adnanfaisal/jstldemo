<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>   
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<c:set var="theLocale" 
value="${not empty param.theLocale ? param.theLocale : pageContext.request.locale}" 
scope="session" /> 

<fmt:setLocale value="${theLocale}"/> 
<fmt:setBundle basename="com.adnan.i18n.resource.mylabels" />

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">


<title>Carleton University</title>
</head>
<body>

<a href="i18n-messages-test.jsp?theLocale=en_CA"> English (CA)</a> |
<a href="i18n-messages-test.jsp?theLocale=it_IT"> Italian (It)</a> | 
<a href="i18n-messages-test.jsp?theLocale=de_DE"> German (DE)</a>

<hr>

<p>
<fmt:message key="label.greeting"> </fmt:message> <br>
<fmt:message key="label.firstName"> Adnan </fmt:message> <br>
<fmt:message key="label.lastName"> Faisal </fmt:message> <br>
<fmt:message key="label.welcome"> </fmt:message>  <br>
</p>
<hr>
Selected language: ${theLocale}
</body>
</html>