<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>   

<% 
	String[] cities = {"Dhaka", "Ottawa", "Doha"}; 
	pageContext.setAttribute("myCities", cities);
%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>


My cities are:

<c:forEach var="city" items="${myCities}"> 
	${city} 
</c:forEach>

</body>
</html>