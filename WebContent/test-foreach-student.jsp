<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@ page import="java.util.*, com.adnan.model.Student" %>  

<% 
	List <Student> students = new ArrayList<>(); 
	
	students.add(new Student("Adnan","Faisal",true));
	students.add(new Student("Farhana","Islam",true));
	students.add(new Student("Md","Bashar",false));
	
	pageContext.setAttribute("students", students);
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<table border=1>
	<tr>
		<th> First Name </th>
		<th> Last Name </th>
		<th> Gold Customer </th>		
	</tr>
<c:forEach var="student" items="${students}">
	<tr>
		<td>${student.firstName} 
		<td> ${student.lastName} 
		<td> 
		<c:if test="${student.goldCustomer}">
			Special Discount
		</c:if>
		<c:if test="${not student.goldCustomer}">
			No Discount
		</c:if>
		
	</tr>
</c:forEach>
</table>
</body>
</html>